## Modul zur Konvertierung hochgeladener Video-Files ##

 Der Videoencoder prüft für alle neu auf dem Server bereitgestellten Videodateien, ob
 Container-Format,Video- und Audiocodec mit den spezifizierten Zielplattformen
 kompatibel sind. Ergibt die Prüfung eine Inkompatibilität, wird die entsprechende
 Datei in ein kompatibles Format formatiert und falls nötig Video- sowie Audiocodec neu
 kodiert. 

 Dabei startet das Skript "videoencoder.php" in einem eigenen Prozess und beginnt erst 
 nach Abschluss aller früheren Aufträge mit der Encodierung. 

 Zielplattformen:

 >   Android ab 2.4.1, IOS (IPHONE 4), aktuelle Browser ab IE9+/Chrome/FireFox

 Zielformat: 
 	- Container: mp4
 	- Video-Codec: h264
 	- Audio-Codec: acc  

 Änderungen des Zielformat sind über Anpassung der Werte im Abschnitt CONSTANTS &
 SETTINGS möglich (z.B. TARGET_VIDEO_CODEC).

---------------------------

### Veröffentlichung & Lizenzierung ### 

Alle Ergebnisse des Praxisprojekts sind hiermit veröffentlicht und können unter Beachtung des folgenden Lizenztextes von jederman frei verwendet werden.
```
Copyright (c) 2016, VAPPS Projektteam der FH-Bingen Proj-WS1516

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```
----------------------
Alexander Weiß 26.01.2016
