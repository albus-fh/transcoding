<?php
/**
 * ISC License (ISC)
 *
 * Copyright (c) $yaer.today, Alexander Weiß <info@albus-it.com>member of the VAPPS-Team
 * (FH-Bingen) Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD
 * TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN
 * NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/**
 * Modul zur Konvertierung hochgeladener Video-Files
 *
 * Der Videoencoder prüft für alle neu auf dem Server bereitgestellten Videodateien, ob
 * Container-Format,Video- und Audiocodec mit den spezifizierten Zielplattformen
 * kompatibel sind. Ergibt die Prüfung eine Inkompatibilität, wird die entsprechende
 * Datei in ein kompatibles Format formatiert und falls nötig Video- sowie Audiocodec neu
 * kodiert.
 *
 * Zielplattformen:
 *
 *          Android ab 2.4.1, IOS (IPHONE 4), aktuelle Browser ab IE9+/Chrome/FireFox
 *
 * Bestmögliche Kompatibilität bietet ein mp4-Container mit einem h264-Videocodec und
 * einer AAC-Audiospur; Lizenzbedingungen beachten:
 * http://www.mpegla.com/main/default.aspx
 * (http://www.heise.de/newsticker/meldung/Lizenzbedingungen-fuer-Videocodec-H-264-AVC-veroeffentlicht-88771.html)
 *
 * 2. Wahl ist das lizenzfreie webm-format (Container) mit einer V8-Video und
 * Vobis-Audiocodec. Nachteil: Möglicherweise nicht mit IOS (IPhone4) kompatibel.
 * Zusätzlich könnte für Dateien, die nicht schon zufällig in mp4-Format vorliegen, eine
 * ios-kompatible Kopie des Videos gespeichert werden.
 *
 * Änderungen des Zielformat sind über Anpassung der Werte im Abschnitt CONSTANTS &
 * SETTINGS möglich (z.B. TARGET_VIDEO_CODEC).
 *
 * Alexander Weiß 26.01.2016
 *
 * --------------------------------------------------------------------------------------
 */

/** @type file $DB_PASSWD_INI */
$DB_PASSWD_INI = 'host=localhost port=5432 dbname=placity user=placity password=placity';
/** @type String $FILE_SERVER */
$FILE_SERVER = '/opt/pc4/df2/storage/app/';
/** @type String $TARGET_VIDEO_CODEC */
$TARGET_VIDEO_CODEC = 'libx264';
/** @type String $TARGET_AUDIO_CODEC */
$TARGET_AUDIO_CODEC = 'aac';
/** @type String $TARGET_FILE_CONTAINER */
$TARGET_FILE_CONTAINER = 'mp4';
$TARGET_SCALE = '640x480';
$jobQueue = [];


new Transcoder();

class Transcoder
{
    public $files;
    public $db;

    /**
     * Transcoder constructor.
     */
    public function __construct()
    {
        global $DB_PASSWD_INI, $FILE_SERVER, $TARGET_VIDEO_CODEC, $TARGET_AUDIO_CODEC;
        global $TARGET_FILE_CONTAINER, $TARGET_SCALE, $jobQueue;

        $this->db = pg_connect($DB_PASSWD_INI);
        $this->files = pg_query(pg_connect($DB_PASSWD_INI),
                                "select id,file_name,path,url
                    from mediafile
                    WHERE transcoded = FALSE
                    and transcoding = FALSE
                    and file_type like '%video%';");

        $this->files = $this->files == null ? [] : $this->files;
        $this->run();
    }


    /**
     * @inheritdoc
     */
    public function run()
    {
        global $DB_PASSWD_INI, $FILE_SERVER, $TARGET_VIDEO_CODEC, $TARGET_AUDIO_CODEC;
        global $TARGET_FILE_CONTAINER, $TARGET_SCALE, $jobQueue;

      

        foreach ($this->files as $file) {
            $info = new ffprobe($file->path . $file->file_name);

            foreach ($info->streams as $stream) {
                if (
                    ($info->format->format_name != 'mov,mp4,m4a,3gp,3g2,mj2')
                    || ($stream->codec_type == 'video' && $stream->codec_name != 'h264')
                    || ($stream->codec_type == 'audio' && $stream->codec_name != 'acc')
                ) {
                    pg_query($this->db,
                             "UPDATE mediafile SET filesize=$info->format->size,transcoding=true WHERE id=$file->id");

                    //Lock zur Synchronisation
                    while (file_exists('lock_jobQueue')) {
                        sleep(1);
                    }
                    shell_exec("echo lock_jobQueue > lock_jobQueue");

                    array_push($jobQueue, $file);

                    shell_exec("rm lock_jobQueue");
                }
            }
        }


        while (file_exists('lock_run')) {
            sleep(1);
        }
        shell_exec("echo lock_run > lock_run");


        $file = array_pop($jobQueue);
        while ($file != null) {
            shell_exec("echo lock_jobQueue > lock_jobQueue");
            while (file_exists('lock_jobQueue')) {
                sleep(1);
            }
            shell_exec(
                "ffmpeg -i" . $file . " -c:v " . $TARGET_VIDEO_CODEC
                . " -c:a " . $TARGET_AUDIO_CODEC . " -c:s copy -s "
                . $TARGET_SCALE . " -moveflag + faststart  -f "
                . $TARGET_FILE_CONTAINER . " "
                . $file->path . $file . file_name . "mp4"
            );

            pg_query($this->db,
                     "UPDATE mediafile SET file_name=$file->file_name
                      .'mp4',url=$file->url.'mp4',transcoded=true  WHERE id=$file->id");

            shell_exec("rm lock_jobQueue");
        }
        shell_exec("rm lock_run");
    }
}


/**
 * ffprobe helper class
 * Created by PhpStorm.
 * User: Alexander Weiß       Date: 16.02.2016
 * File: ffprobe.php
 *
 */
class ffprobe
{
    public function __construct($filename, $prettify = false)
    {
        if (!file_exists($filename)) {
            throw new Exception(sprintf('File not exists: %s', $filename));
        }
        $this->__metadata = $this->__probe($filename, $prettify);
    }

    private function __probe($filename, $prettify)
    {
        $init = microtime(true);
        // Default options
        $options = '-loglevel quiet -show_format -show_streams -print_format json';
        if ($prettify) {
            $options .= ' -pretty';
        }
        setlocale(LC_CTYPE, 'en_EN.UTF-8');
        $json = json_decode(shell_exec(sprintf('ffprobe %s %s', $options,
                                               escapeshellarg($filename))));
        if (!isset($json->format)) {
            throw new Exception('Unsupported file type');
        }
        // Save parse time (milliseconds)
        $this->parse_time = round((microtime(true) - $init) * 1000);
        return $json;
    }
}